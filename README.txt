This file contains the scenarios and java code to run the scenarios discussed in the report. The directorys can be inserted into a MATSim project, like https://github.com/matsim-org/matsim-example-project for example, and can then be run from there. Following there will be a short explanation for each software artefact.


scenarios\quadrate_freight_central
This scenario contains the files necessary to run the scenario with central pick-up stations. It is a modified version of the exemplary code provided in https://github.com/matsim-org/matsim-libs/tree/master/examples/scenarios/freight-chessboard-9x9. These files can be run with the RunQuadrateFreightCentral java file.

scenarios\quadrate_freight_normal
This scenario contains the files necessary to run the scenario with normal pick-up stations. It is a modified version of the exemplary code provided in https://github.com/matsim-org/matsim-libs/tree/master/examples/scenarios/freight-chessboard-9x9. These files can be run with the RunQuadrateFreightNormal java file.

scenarios\quadrate_nofreight
This scenario contains the files necessary to run the scenario without the freight extension.


src\main\java\org\matsim\project\RunQuadrateFreightCentral
This is the java code necessary to run the quadrate_freight_central scenario. It is a copy of the freight example found on https://github.com/matsim-org/matsim-code-examples/tree/15.x/src/main/java/org/matsim/codeexamples/extensions/freight by Prof. Nagel which has been modiefied to run this scenario.


src\main\java\org\matsim\project\RunQuadrateFreightNormal
This is the java code necessary to run the quadrate_freight_normal scenario. It is a copy of the freight example found on https://github.com/matsim-org/matsim-code-examples/tree/15.x/src/main/java/org/matsim/codeexamples/extensions/freight by Prof. Nagel which has been modiefied to run this scenario.
